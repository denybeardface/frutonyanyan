import { Sprite, Container, TextStyle, Text } from "pixi.js";
import TweenMax, { TimelineMax, Power0 } from "gsap/TweenMax";
import { Howl, Howler } from "howler";
import ft from "./FairyTales";

const { innerWidth, innerHeight } = window;
const getAspectRatio = () => innerHeight / innerWidth;
const getGlobalScale = () => {
  if (innerWidth > 400) return 0.8;
  else return innerHeight < 600 ? 0.8 : 0.9;
};
const playBtn = document.querySelector("#playBtn");
const ind = document.querySelector("#stateIndicator");
const path = document.querySelector(".path");
const msgStyle = new TextStyle({
  fontFamily: "SansRoundedC",
  fontSize: 22,
  align: "center",
  fill: "#007ac5"
});

const msgHeaderStyle = new TextStyle({
  fontFamily: "SansRoundedC",
  fontSize: 32,
  align: "center",
  fill: "#ffffff",
  letterSpacing: 1
});
const STATE_ICONS = {
  PLAY: "images/play.png",
  PAUSE: "images/pause.png",
  REC: "images/rec.png"
};

const loadAssets = ({ path, frames, alias, loader, ext }) => {
  for (let i = 0; i < frames; i++) {
    loader.add(
      `${alias}${i}`,
      `${path}/${alias}${i < 10 ? "0" + i : i}.${ext}`
    );
  }
};

const text2pages = text => {
  const words = text.split(" ");
  const pages = [];
  let line = "";
  let count = 1;

  words.forEach(word => {
    word.replace(/(\r\n|\n|\r)/gm, "");
    word.replace(/(\u8629)/gm, "");
    word = word.trim();
    const shouldTerminate = line.length + word.length > count * 15;

    if (shouldTerminate) {
      if (count == 3) {
        pages.push(line);
        line = word + " ";
        count = 1;
      } else {
        line = line + "\n" + word + " ";
        count++;
      }
    } else {
      line += word + " ";
    }
  });

  pages.push(line);
  return pages;
};

if (getGlobalScale() === 0.8) {
  playBtn.classList.add("scaled");
}

export default class Game {
  constructor(app, stage) {
    this.app = app;
    this.sounds = {
      start1: new Howl({
        src: "audio/start1.mp3"
      }),
      start2: new Howl({
        src: "audio/start2.mp3"
      }),
      igra2: new Howl({
        src: "audio/igra2.mp3"
      }),
      igra3: new Howl({
        src: "audio/igra3.mp3"
      }),
      igra4: new Howl({
        src: "audio/igra4.mp3"
      }),
      calm: new Howl({
        src: "audio/lullaby.mp3",
        loop: true
      }),
      game: new Howl({
        src: "audio/game.mp3",
        loop: true
      }),
      ftCow: new Howl({
        src: "audio/ft_cow.mp3",
        volume: 0.5
      }),
      ftOwl: new Howl({
        src: "audio/ft_owl.mp3",
        volume: 0.5
      }),
      main: new Howl({
        src: "audio/main.mp3",
        volume: 0.5
      })
    };
    this.stage = stage;
    this.scene = null;
    this.muted = false;
    this.timelines = [];
    this.rec = null;
    this.file = null;
    this.slizeSize = 1000 * 512;
    this.reader = null;
    this.ft1 = text2pages(ft.cow);
    this.ft2 = text2pages(ft.owl);

    loadAssets({
      path: "stars",
      frames: 90,
      alias: "stars",
      loader: this.app.loader,
      ext: "png"
    });

    loadAssets({
      path: "images/body_wings",
      frames: 30,
      alias: "owl_body",
      loader: this.app.loader,
      ext: "png"
    });

    loadAssets({
      path: "images/beak",
      frames: 15,
      alias: "owl_beak",
      loader: this.app.loader,
      ext: "png"
    });

    this.app.loader
      .add("mainBg", "images/sky2.png")
      .add("bubble_intro", "images/introBubble.png")
      .add("listen", "images/listen.png")
      .add("record", "images/record.png")
      .add("back", "stages/listen/back.png")
      .add("bubble_listen", "stages/listen/bubble.png")
      .add("mute", "images/audio_on.png")
      .add("muteOFF", "images/audio_off.png")
      .add("owl", "images/owl.png")
      .add("pickCow", "images/pickBoy.png")
      .add("pickOwl", "images/pickGirl.png")
      .load(this.fontLoaded);

    this.app.loader.on("progress", loader => {
      document.querySelector(".progress").textContent = `Загрузка ${Math.floor(
        loader.progress
      )} %`;
    });
  }

  fontLoaded = () => {
    window.WebFont.load({
      custom: {
        families: ["SansRoundedC", "SansRoundedLightC"],
        urls: ["https://brainrus.ru/frutonyanyan/fonts/fonts.css"] // prod ..
      },
      active: () => {
        TweenMax.to(".loader", 0.4, { opacity: 0 });
        this.onloadComplete();
      }
    });
  };

  onloadComplete = () => {
    const { stage } = this;
    const intro = this.createIntro;
    // const listen = this.createListenFT;

    this.scenes = {
      intro
      // listen
    };

    this.render(stage);
    //document.querySelector("#pb-wrapper").style.display = "none";
  };

  createBG = () => {
    const { resources } = this.app.loader;
    const container = new Container();
    const bg = new Sprite(resources.mainBg.texture);
    const back_btn = new Sprite(resources.back.texture);
    const mute_btn = new Sprite(resources.mute.texture);

    bg.width = innerWidth;
    bg.height = innerWidth * getAspectRatio() + innerWidth / 2;
    bg.y = -120;

    back_btn.position.set(20, 20);
    mute_btn.position.set(innerWidth - mute_btn.width - 20, 20);

    var i = 0;
    var start = 10;
    var end = 90;

    back_btn.interactive = true;
    back_btn.buttonMode = true;
    mute_btn.interactive = true;
    mute_btn.buttonMode = true;

    mute_btn.on("pointerup", () => {
      this.muted = !this.muted;
      if (this.muted) {
        mute_btn.texture = this.app.loader.resources.muteOFF.texture;
      } else {
        mute_btn.texture = this.app.loader.resources.mute.texture;
      }
      Howler.mute(this.muted);
    });

    back_btn.on("pointerup", () => {
      Object.values(this.sounds).forEach(sound => sound.stop());
      this.timelines.forEach(tml => tml.pause(0));
      const { protocol, hostname, port, pathname } = window.location;
      window.location.href = `${protocol}//${hostname}:${port}${pathname}?action=intro`;
    });

    setInterval(() => {
      switch (this.stage) {
        case "intro":
          [back_btn, mute_btn].map(item => {
            item.interactive = false;
            item.alpha = 0;
          });
          playBtn.style.display = "none";
          break;
        case "record:ready":
          mute_btn.interactive = false;
          mute_btn.alpha = 0;
          playBtn.style.display = "block";
          break;
        case "record:setup":
          mute_btn.interactive = false;
          mute_btn.alpha = 0;
          playBtn.style.display = "none";
          break;
        case "record:finish":
          mute_btn.interactive = false;
          mute_btn.alpha = 0;
          playBtn.style.display = "none";
          break;
        case "record:save":
          mute_btn.interactive = false;
          mute_btn.alpha = 0;
          playBtn.style.display = "block";
          playBtn.classList.add("elevated");
          break;
        case "listen":
          [back_btn, mute_btn].map(item => {
            item.interactive = true;
            item.alpha = 1;
          });
          playBtn.style.display = "block";
          break;
        case "listen:select":
          [back_btn, mute_btn].map(item => {
            item.interactive = true;
            item.alpha = 1;
          });
          mute_btn.alpha = 0;
          playBtn.style.display = "none";
          break;
        default:
          playBtn.classList.remove("elevated");
          playBtn.style.display = "none";

          break;
      }
    }, 200);

    new Promise(res => {
      const swap = () => {
        i += 1;

        if (i >= end) {
          i = start;
        }
        stars.texture = resources[`stars${i}`].texture;
      };

      setInterval(swap, 1000 / 24);
    });

    const stars = new Sprite(resources["stars0"].texture);
    stars.width = innerWidth;
    stars.height = innerWidth * getAspectRatio() + innerWidth / 2;
    stars.y = -80;

    container.addChild(bg);
    container.addChild(stars);
    container.addChild(back_btn);
    container.addChild(mute_btn);

    container.children.map(item => {
      if (item === bg) return;
      item.scale.set(getGlobalScale(), getGlobalScale());
    });

    return container;
  };

  createIntro = () => {
    const { resources } = this.app.loader;
    const container = new Container();
    const owl = new Container();
    const owlBody = new Sprite(resources.owl_body0.texture);
    const owlBeak = new Sprite(resources.owl_beak0.texture);
    const rec_btn = new Sprite(resources.record.texture);
    const listen_btn = new Sprite(resources.listen.texture);
    const bubble = new Sprite(resources.bubble_intro.texture);
    const largeMute = new Sprite(resources.muteOFF.texture);
    const msg = new Text(
      "Давай послушаем\nвместе сказку перед\nсном или ты можешь\nзаписать свою,\nуф-уф",
      msgStyle
    );
    let shouldBeakAnimate = true;
    const introMsg = new Text("Жми, чтобы\nвключить звук", msgHeaderStyle);

    const introCont = new Container();
    introCont.addChild(introMsg);

    introCont.addChild(largeMute);

    largeMute.position.set(largeMute.width, 0);
    introMsg.position.set(0, largeMute.y + largeMute.height + 30);

    introCont.position.set(
      (innerWidth - introCont.width) / 2,
      (innerHeight - introCont.height) / 2
    );
    owl.addChild(owlBody);
    owl.addChild(owlBeak);

    const bodyAnim = () => {
      var frame = 0;
      const int = setInterval(() => {
        frame = frame + 1 === 30 ? 0 : frame + 1;
        owlBody.texture = resources[`owl_body${frame}`].texture;
      }, 1000 / 30);
    };

    const beakAnim = () => {
      var frame = 0;
      const int = setInterval(() => {
        if (shouldBeakAnimate) {
          frame = frame + 1 === 15 ? 0 : frame + 1;
          owlBeak.texture = resources[`owl_beak${frame}`].texture;
        } else {
          owlBeak.texture = resources[`owl_beak7`].texture;
        }
      }, 1000 / 30);
    };

    this.sounds.main.on("end", () => (shouldBeakAnimate = false));
    [(owl, rec_btn, msg, listen_btn, bubble)].map(item =>
      item.scale.set(getGlobalScale(), getGlobalScale())
    );

    bubble.position.set((innerWidth - bubble.width) / 2, 30);
    owl.position.set(20, bubble.y + bubble.height - 50);

    msg.position.set(
      (innerWidth - msg.width) / 2,
      (bubble.y + bubble.height - msg.height) / 2 + 10
    );

    rec_btn.x = (innerWidth - rec_btn.width) / 2;
    rec_btn.y = innerHeight - rec_btn.height - 10;

    listen_btn.x = (innerWidth - listen_btn.width) / 2;
    listen_btn.y = rec_btn.y - listen_btn.height - 10;

    listen_btn.interactive = true;
    listen_btn.buttonMode = true;
    listen_btn.on("pointerup", () => {
      const { protocol, hostname, port, pathname } = window.location;
      window.location.href = `${protocol}//${hostname}:${port}${pathname}?action=listen`;
      listen_btn.interactive = false;
      listen_btn.buttonMode = false;
      rec_btn.interactive = false;
      rec_btn.buttonMode = false;
      window.yaCounter52429498.reachGoal("listen");
    });

    rec_btn.interactive = true;
    rec_btn.buttonMode = true;
    rec_btn.on("pointerup", () => {
      const { protocol, hostname, port, pathname } = window.location;
      window.location.href = `${protocol}//${hostname}:${port}${pathname}?action=record`;
      listen_btn.interactive = false;
      listen_btn.buttonMode = false;
      rec_btn.interactive = false;
      rec_btn.buttonMode = false;
      window.yaCounter52429498.reachGoal("write");
    });

    [owl, bubble, msg, rec_btn, listen_btn].map(item => (item.visible = false));

    largeMute.interactive = true;
    largeMute.buttonMode = true;

    largeMute.on("pointerup", () => {
      window.yaCounter52429498.reachGoal("turn_sound");
      this.sounds.main.play();
      bodyAnim();
      beakAnim();
      [owl, bubble, msg, rec_btn, listen_btn].map(
        item => (item.visible = true)
      );
      introCont.visible = false;
      largeMute.interactive = false;
    });
    container.addChild(owl);
    container.addChild(bubble);
    container.addChild(msg);
    container.addChild(rec_btn);
    container.addChild(listen_btn);
    container.addChild(introCont);
    return container;
  };

  // createListenFT = () => {
  //   const { resources } = this.app.loader;
  //   const { igra2 } = this.sounds;

  //   const container = new Container();
  //   const owl = new Container();
  //   const owlBody = new Sprite(resources.owl_body0.texture);
  //   const owlBeak = new Sprite(resources.owl_beak0.texture);
  //   const bubble = new Sprite(resources.bubble_listen.texture);
  //   const msg = new Text(
  //     "Я готов\nслушать сказку,\nнажимай на кнопку",
  //     msgStyle
  //   );
  //   const pickCow = new Sprite(resources.pickCow.texture);
  //   const pickOwl = new Sprite(resources.pickOwl.texture);
  //   const pickFTHeader = new Text("Выбери сказку", msgHeaderStyle);

  //   let shouldBeakAnimate = false;
  //   this.stage = "listen:select";

  //   owl.addChild(owlBody);
  //   owl.addChild(owlBeak);

  //   const bodyAnim = () => {
  //     var frame = 0;
  //     const int = setInterval(() => {
  //       frame = frame + 1 === 30 ? 0 : frame + 1;
  //       owlBody.texture = resources[`owl_body${frame}`].texture;
  //     }, 1000 / 30);
  //   };

  //   const beakAnim = () => {
  //     var frame = 0;
  //     const int = setInterval(() => {
  //       if (shouldBeakAnimate) {
  //         frame = frame + 1 === 15 ? 0 : frame + 1;
  //         owlBeak.texture = resources[`owl_beak${frame}`].texture;
  //       } else {
  //         owlBeak.texture = resources[`owl_beak7`].texture;
  //       }
  //     }, 1000 / 60);
  //   };

  //   bodyAnim();
  //   beakAnim();

  //   container.addChild(owl);
  //   container.addChild(bubble);
  //   container.addChild(msg);
  //   container.addChild(pickCow);
  //   container.addChild(pickOwl);
  //   container.addChild(pickFTHeader);

  //   pickCow.interactive = true;
  //   pickCow.buttonMode = true;
  //   pickOwl.interactive = true;
  //   pickOwl.buttonMode = false;

  //   const showListen = () => {
  //     try {
  //       shouldBeakAnimate = true;
  //       igra2.play();
  //       igra2.on("end", () => (shouldBeakAnimate = false));
  //       owl.visible = true;
  //       bubble.visible = true;
  //       msg.visible = true;

  //       pickFTHeader.visible = false;
  //       pickCow.visible = false;
  //       pickOwl.visible = false;
  //       this.stage = "listen";

  //       let page = 0;
  //       let currentTime = 0;
  //       const dura = this.activeFT === this.ft1 ? 163 : 169;
  //       const pps = (dura * 1000) / this.activeFT.length;
  //       const autotext = setInterval(() => {
  //         try {
  //           if (!this.activeSound.playing()) return;
  //           if (currentTime > pps * (page + 1)) {
  //             page++;
  //             msg.text = this.activeFT[page];
  //             msg.position.set(
  //               bubble.x + (bubble.width - msg.width) / 2,
  //               bubble.y + (bubble.height - msg.height) / 2 - 15
  //             );
  //           }

  //           currentTime += 110;
  //         } catch (err) {
  //           console.log(err);
  //         }
  //       }, 110);

  //       const ptml = new TimelineMax({ paused: true });
  //       ptml.to(path, dura, {
  //         strokeDashoffset: 0,
  //         ease: Power0.easeNone,
  //         onUpdate: () => {
  //           try {
  //             if (this.stage !== "listen") {
  //               ind.src = STATE_ICONS.PLAY;
  //               ptml.pause(0);
  //             }
  //           } catch (err) {
  //             console.log(err);
  //           }
  //         }
  //       });
  //       this.timelines.push(ptml);

  //       playBtn.addEventListener("click", () => {
  //         if (this.stage !== "listen") return;
  //         igra2.pause();
  //         shouldBeakAnimate = false;
  //         msg.text = this.activeFT[page];
  //         msg.position.set(
  //           bubble.x + (bubble.width - msg.width) / 2,
  //           bubble.y + (bubble.height - msg.height) / 2 - 15
  //         );
  //         msg.style.fontSize = 23;
  //         if (this.activeSound.playing()) {
  //           ptml.pause();
  //           this.activeSound.pause();
  //           ind.src = STATE_ICONS.PLAY;
  //         } else {
  //           ind.src = STATE_ICONS.PAUSE;
  //           ptml.play();
  //           this.activeSound.play();
  //         }
  //       });

  //       this.activeSound.on("end", () => {
  //         ptml.pause(0);
  //         ind.src = STATE_ICONS.PLAY;
  //         clearInterval(autotext);
  //         page = 0;
  //         setTimeout(() => {
  //           window.location.href =
  //             "https://brainrus.ru/frutonyanyan?action=intro";
  //         }, 5000);
  //       });
  //     } catch (err) {
  //       console.log(err);
  //     }
  //   };

  //   pickCow.on("pointerup", () => {
  //     this.activeSound = this.sounds.ftOwl;
  //     this.activeFT = this.ft2;
  //     showListen();
  //   });

  //   pickOwl.on("pointerup", () => {
  //     this.activeSound = this.sounds.ftCow;
  //     this.activeFT = this.ft1;
  //     showListen();
  //   });

  //   this.scaleContainerItems(container);

  //   owl.position.set(30, (innerHeight - owl.height) / 2 + 80);

  //   bubble.position.set(30, owl.y - bubble.height + 30);

  //   msg.position.set(
  //     bubble.x + (bubble.width - msg.width) / 2,
  //     bubble.y + (bubble.height - msg.height) / 2 - 15
  //   );

  //   pickFTHeader.position.set((innerWidth - pickFTHeader.width) / 2, 120);

  //   pickOwl.position.set(
  //     (innerWidth - pickOwl.width) / 2,
  //     innerHeight - pickOwl.height - 100
  //   );

  //   pickCow.position.set(
  //     (innerWidth - pickCow.width) / 2,
  //     pickOwl.y - pickCow.height - 30
  //   );

  //   owl.visible = false;
  //   bubble.visible = false;
  //   msg.visible = false;

  //   return container;
  // };

  setScene = key => {
    if (this.scene) {
      this.app.stage.children.map(child => {
        if (child === this.scene) {
          this.app.stage.removeChild(child);
        }
      });
    }
    this.stage = key;
    key = key.split(":").length ? key.split(":")[0] : key;
    this.scene = this.scenes[key];
    const bg = this.createBG();
    this.app.stage.addChild(bg);
    this.app.stage.addChild(this.scenes[key]());
  };

  scaleContainerItems = container => {
    container.children.map(item => {
      const newScale = innerWidth > 400 ? 0.8 : getGlobalScale();
      item.scale.set(newScale, newScale);
    });
  };

  toggle = item => {
    item.visible = !item.visible;
    item.interactive = !item.interactive;
    item.buttonMode = !item.buttonMode;
  };

  render = () => {
    const { stage } = this;
    console.log(stage);
    this.setScene(stage);
  };
}
