import { Howl } from "howler";
import RecordRTC, { StereoAudioRecorder } from "recordrtc";
import * as $ from "jquery";

class Recorder {
  constructor() {
    this.rec = null;
    this.file = null;
    this.tmp_sound = null;
    this.int = null;
    this.slizeSize = 1000 * 512;
    this.reader = null;
    this.progress = 0;

    $(window).on("blur", () => {
      this.toggleMicrophone();
    });

    $(window).on("focus", () => {
      this.toggleMicrophone();
    });
  }

  getMic = () => {
    if (!navigator.getUserMedia && !navigator.mediaDevices) {
      console.log("guess I suck");
    }

    if (!navigator.mediaDevices) {
      navigator.getUserMedia(
        {
          audio: true,
          echoCancellation: true
        },
        this.handleMedia
      );
    } else {
      navigator.mediaDevices
        .getUserMedia({
          audio: true,
          echoCancellation: true
        })
        .then(this.handleMedia)
        .catch(err => console.log(err));
    }
  };

  handleMedia = stream => {
    if (!this.rec) {
      this.rec = RecordRTC(stream, {
        type: "audio",
        mimeType: "audio/webm",
        recorderType: StereoAudioRecorder
      });
    }
  };

  saveFile = () => {
    const { rec } = this;
    rec && rec.stopRecording(this.handleStop);
  };

  handleStop = () => {
    try {
      const file = this.rec.getBlob();
      this.file = new File([file], "file.wav", { lastModified: new Date() });

      this.token = Date.now() + "s" + Math.ceil(Math.random() * 1000);
      this.tmp_sound = new Howl({
        src: [URL.createObjectURL(file)],
        format: file.type.split("/")[1]
      });
    } catch (err) {
      console.log(err);
    }
  };

  uploadChunk = (start, cb) => {
    const nextSlice = start + this.slizeSize + 1;
    console.log("next slice len is ", nextSlice);
    const blob = this.file.slice(start, nextSlice);
    const complete = start + this.slizeSize >= this.file.size;

    this.reader.onloadend = event => {
      if (event.target.readyState !== FileReader.DONE) {
        return;
      }
      console.log(this.file);
      $.ajax({
        url: "https://brainrus.ru/upload",
        type: "POST",
        dataType: "json",
        cache: false,
        data: {
          file_data: event.target.result,
          file: this.file.name,
          file_type: this.file.type,
          token: this.token,
          complete: complete
        },
        error: (jqXHR, textStatus, err) => {
          console.log(textStatus, err);
        },
        success: data => {
          var size_done = start + this.slizeSize;
          console.log(start, this.slizeSize, size_done);
          var percent_done = Math.floor((size_done / this.file.size) * 100);
          if (nextSlice < this.file.size) {
            console.log("Uploading File - " + percent_done + "%");
            this.progress = percent_done;
            this.uploadChunk(nextSlice, cb);
          } else {
            console.log("upload complete", data);
            this.url = data.url;
            cb();
          }
        }
      });
    };
    this.reader.readAsDataURL(blob);
  };

  startUpload = cb => {
    this.reader = new FileReader();
    this.uploadChunk(0, cb);
  };

  toggleMicrophone = () => {
    const { rec } = this;
    if (rec) {
      console.log(rec.state);
      if (rec.state === "inactive" || rec.state === "stopped") {
        rec.startRecording();
        return;
      }
      rec.state === "recording" ? rec.pauseRecording() : rec.resumeRecording();
    }
  };
}

export default Recorder;
