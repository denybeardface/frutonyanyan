import * as $ from "jquery";
import { Howl } from "howler";
import TweenMax, { TimelineMax, Power0 } from "gsap/TweenMax";
import * as PIXI from "pixi.js";
import clipboard from "clipboard";

import "./App.css";
import ft from "./FairyTales";
import Recorder from "./recorder";
import Game from "./Game";

const search = window.location.search.substr(1).split("=")[1];
const pixiRoot = $("#pixi").get(0);
const root = $("#root");
window.$ = $;
const imgW = 1080;
const imgH = 2340;

const contW = window.innerWidth;
const contH = window.innerHeight;

const imgRatio = imgH / imgW;
const contRatio = contH / contW;
const playBtn = $("#playBtn");
const pickStory = $("#pickStory");
const pickMusic = $("#pickMusic");
const record = $("#record");
const recordFinish = $("#recordFinish");
const share = $("#share");
const show = { display: "flex" };
const ind = $("#stateIndicator").get(0);
const path = $(".path").get(0);
const cowBtn = $("#cow");
const owlBtn = $("#owl");
const calmBtn = $("#calm");
const funBtn = $("#fun");
const reader = record.find("#reader");
const readerText = reader.find("#text");
const next = reader.find("#right");
const prev = reader.find("#left");
const finishBtn = $("#finish");
const saveBtn = $("#saveBtn");
const copyBtn = $("#copyBtn");
const backBtn = $("#backBtn");
const gameState = {
  story: null,
  sound: null,
  fairyTale: [],
  page: 0
};
const gotoPage = page => {
  return gameState.fairyTale[page] || [];
};
const rec = new Recorder();
let activeSound,
  ltml,
  starIdx = 0;

const send = tag => {
  console.log("sending " + tag + " event");
  window.yaCounter52429498 && window.yaCounter52429498.reachGoal(tag);
};

const sendOnce = tag => {
  var sent = false;

  const send = s => {
    if (!sent)
      window.yaCounter52429498 && window.yaCounter52429498.reachGoal(s);
    sent = true;
  };
  send(tag);
};

$(window).on("load", () => {
  $(".loader").hide();

  setInterval(() => {
    starIdx = starIdx + 1 > 90 ? 0 : starIdx + 1;
    $(".starsImg").get(0).src = `https://brainrus.ru/frutonyanyan/stars2/stars${
      starIdx < 10 ? "0" + starIdx : starIdx
    }.png`;
    // $(".starsImg").get(0).style.backgroundImage = `url('stars/stars${
    //   starIdx < 10 ? "0" + starIdx : starIdx
    // }.png')`;
  }, 1000 / 5);
});
const sounds = {
  start1: new Howl({
    src: "audio/start1.mp3"
  }),
  start2: new Howl({
    src: "audio/start2.mp3"
  }),
  igra2: new Howl({
    src: "audio/igra2.mp3"
  }),
  igra3: new Howl({
    src: "audio/igra3.mp3"
  }),
  igra4: new Howl({
    src: "audio/igra4.mp3"
  }),
  calm: new Howl({
    src: "audio/lullaby.mp3",
    loop: true,
    volume: 0.4
  }),
  game: new Howl({
    src: "audio/game.mp3",
    loop: true,
    volume: 0.4
  }),
  ftCow: new Howl({
    src: "audio/ft_cow.mp3",
    volume: 0.5
  }),
  ftOwl: new Howl({
    src: "audio/ft_owl.mp3",
    volume: 0.5
  })
};
let finalHeight, finalWidth;

if (contRatio > imgRatio) {
  finalHeight = contH;
  finalWidth = contH / imgRatio;
} else {
  finalWidth = contW;
  finalHeight = contW * imgRatio;
}

$(".starsImg").css({
  width: finalWidth,
  height: finalHeight
});

const STATE_ICONS = {
  PLAY: "images/play.png",
  PAUSE: "images/pause.png",
  REC: "images/rec.png"
};
const text2pages = text => {
  const words = text.split(" ");
  const pages = [];
  let line = "";
  let count = 1;

  words.forEach(word => {
    word.replace(/(\r\n|\n|\r)/gm, "");
    word.replace(/(\u8629)/gm, "");
    word = word.trim();
    const shouldTerminate = line.length + word.length > count * 15;

    if (shouldTerminate) {
      if (count === 6) {
        pages.push(line);
        line = word + " ";
        count = 1;
      } else {
        line = line + "\n" + word + " ";
        count++;
      }
    } else {
      line += word + " ";
    }
  });

  pages.push(line);
  pages.push("Конец!");
  return pages;
};

if (search === "intro") {
  const { innerWidth, innerHeight } = window;
  const width = innerWidth > 414 ? 414 : innerWidth;
  root.hide();
  const app = new PIXI.Application({
    width: width,
    height: innerHeight,
    resolution: devicePixelRatio,
    autoResize: true
  });

  new Game(app, search);
  pixiRoot.appendChild(app.view);
} else if (search === "listen") {
  let activeFT = text2pages(ft.cow);
  let activeSnd;
  let page = 0;
  const dura = activeFT === ft.cow ? 175 : 195;
  const ptml = new TimelineMax({ paused: true });
  ptml.to(path, dura, {
    strokeDashoffset: 0,
    ease: Power0.easeNone
  });
  const runAutoFT = () => {
    let currentTime = 0;
    const pps = (dura * 1000) / activeFT.length;
    const autotext = setInterval(() => {
      try {
        if (!activeSnd.playing()) return;
        if (currentTime > pps * (page + 1)) {
          page++;
          $("#textBubble span").text(activeFT[page]);
        }

        currentTime += 110;
      } catch (err) {
        console.log(err);
      }
    }, 110);

    activeSnd.on("end", () => {
      ptml.pause(0);
      ind.src = STATE_ICONS.PLAY;
      clearInterval(autotext);
      page = 0;
      send("listen_all");
      setTimeout(() => {
        window.location.href = "https://brainrus.ru/frutonyanyan?action=intro";
      }, 5000);
    });
  };

  $(".stageRecord").hide();
  $(window).on("load", () => {
    $(".loader").hide();
    $(".stageListen").show();
    $("#pickStory2.layer").css({ display: "flex" });

    $("#cow2").on("click", () => {
      $("#pickStory2").hide();
      $("#listenStory").css({ display: "flex" });
      playBtn.show();
      activeFT = text2pages(ft.owl);
      activeSnd = sounds.ftOwl;
      runAutoFT();
      sounds.igra2.play();
      send("screen_i_am_ready");
      shouldBeakAnimate = true;
    });

    $("#owl2").on("click", () => {
      $("#pickStory2").hide();
      $("#listenStory").css({ display: "flex" });
      playBtn.show();
      activeFT = text2pages(ft.cow);
      activeSnd = sounds.ftCow;
      runAutoFT();
      sounds.igra2.play();
      send("screen_i_am_ready");
      shouldBeakAnimate = true;
    });
    sounds.igra2.on("end", () => (shouldBeakAnimate = false));
    const owlBody = $("#body").get(0);
    const owlBeak = $("#beak").get(0);
    const bodyAnim = () => {
      var frame = 0;
      const int = setInterval(() => {
        frame = frame + 1 === 30 ? 0 : frame + 1;
        owlBody.src = `https://brainrus.ru/frutonyanyan/images/body_wings/owl_body${
          frame < 10 ? "0" + frame : frame
        }.png`;
      }, 1000 / 30);
    };

    let shouldBeakAnimate = false;
    backBtn.on("click", () => {
      console.log("click");
      const { protocol, host, pathname } = window.location;
      window.location.href = `${protocol}//${host}${pathname}/?action=intro`;
    });
    const beakAnim = () => {
      var frame = 0;
      const int = setInterval(() => {
        if (shouldBeakAnimate) {
          frame = frame + 1 === 15 ? 0 : frame + 1;
          owlBeak.src = `https://brainrus.ru/frutonyanyan/images/beak/owl_beak${
            frame < 10 ? "0" + frame : frame
          }.png`;
        } else {
          owlBeak.src =
            "https://brainrus.ru/frutonyanyan/images/beak/owl_beak07.png";
        }
      }, 1000 / 60);
    };

    bodyAnim();
    beakAnim();

    playBtn.on("click", () => {
      // if (this.stage !== "listen") return;
      sendOnce("listen_play");
      sounds.igra2.pause();
      shouldBeakAnimate = false;
      $("#textBubble span").text(activeFT[page]);
      if (activeSnd.playing()) {
        ptml.pause();
        activeSnd.pause();
        ind.src = STATE_ICONS.PLAY;
        send("listen_pause");
      } else {
        ind.src = STATE_ICONS.PAUSE;
        ptml.play();
        activeSnd.play();
      }
    });
  });
} else {
  send("write_tale");

  rec.getMic();

  $(".stageListen").hide();
  pickStory.css(show);
  cowBtn.on("click", () => {
    gameState.story = "owl";
    gameState.sound = 0;
    send("write_chose_owl");
    pickStory.hide();
    record.css(show);
    $("#recOwl").hide();

    playBtn.show();
    ind.src = STATE_ICONS.REC;
    gameState.stage = "ready";
  });

  owlBtn.on("click", () => {
    gameState.story = "cow";
    gameState.sound = 0;
    send("write_chose_cow");
    pickStory.hide();
    $("#recOwl").hide();

    record.css(show);
    playBtn.show();
    ind.src = STATE_ICONS.REC;
    gameState.stage = "ready";
  });

  $("#gotoRec").on("click", () => {
    send("write_chose_my");
    gameState.story = "none";
    gameState.sound = 0;
    pickStory.hide();
    record.css(show);
    $("#recOwl").hide();
    playBtn.show();
    ind.src = STATE_ICONS.REC;
    gameState.stage = "ready";
  });

  calmBtn.on("click", () => {
    gameState.sound = 0;

    pickMusic.hide();
    record.css(show);
    playBtn.show();
    ind.src = STATE_ICONS.REC;
    gameState.stage = "ready";
  });

  funBtn.on("click", () => {
    gameState.sound = 1;

    pickMusic.hide();
    record.css(show);
    playBtn.show();
    ind.src = STATE_ICONS.REC;
    gameState.stage = "ready";
  });

  finishBtn.on("click", () => {
    record.hide();
    recordFinish.css(show);
    playBtn.addClass("elevated").show();
    send("write_ready");
    ind.src = STATE_ICONS.PLAY;
  });

  saveBtn.on("click", () => {
    gameState.stage = "share";
    send("write_save");
    saveBtn.css({ pointerEvents: "none" });
    const int2 = setInterval(() => {
      if (rec.progress === 100) clearInterval(int2);
      saveBtn.text(`Загружено ${rec.progress}%`);
    }, 100);

    rec.startUpload(() => {
      saveBtn.css({ pointerEvents: "all" });
      recordFinish.hide();
      share.css(show);
      playBtn.hide();
      playBtn.css({ pointerEvents: "none" });

      const shareLinks = [
        `https://vk.com/share.php?url=https://brainrus.ru/${
          rec.url
        }&noparse=true`,
        `https://www.facebook.com/sharer/sharer.php?u=https://brainrus.ru/${
          rec.url
        }`,
        `https://telegram.me/share/url?url=https://brainrus.ru/${rec.url}`,
        `whatsapp://send?text=https://brainrus.ru/${rec.url}`
      ];

      Array.from($("#shareBtns").get(0).children).map((link, idx) => {
        switch (idx) {
          case 0:
            send("write_share_vk");
            break;
          case 1:
            send("write_share_fb");
            break;
          case 2:
            send("write_share_tm");
            break;
          case 3:
            send("write_share_wp");
            break;
          default:
            break;
        }
        link.href = shareLinks[idx];
      });
    });
  });

  playBtn.on("click", e => {
    sendOnce("write_play");
    switch (gameState.stage) {
      case "save":
        checkRec();
        break;
      case "ready":
        startRec();
        break;
      case undefined:
      case "share":
        break;
      default:
        toggleRec();
        break;
    }
  });

  copyBtn.on("click", () => {
    const clipBtn = document.querySelector("#clipboard");
    var cp = new clipboard("#clipboard", {
      text: () => {
        return `https://brainrus.ru/${rec.url}`;
      }
    });
    send("write_copy_url");
    cp.on("success", e => {
      document.querySelector(".notification").classList.add("visible");
      setTimeout(
        () =>
          document.querySelector(".notification").classList.remove("visible"),
        1000
      );
    });

    clipBtn.click();
  });

  backBtn.on("click", () => {
    console.log("click");
    const { protocol, host, pathname } = window.location;
    send("back");
    window.location.href = `${protocol}//${host}${pathname}/?action=intro`;
  });

  const ptml = new TimelineMax({ paused: true });
  ptml.to(path, 180, {
    strokeDashoffset: 0,
    ease: Power0.easeNone,
    onComplete: () => {
      ptml.pause(0);
      finishBtn.show();
      playBtn.hide();
      rec.saveFile();
      activeSound.pause();
      gameState.stage = "save";
      finishBtn.css({ filter: "contrast(50%)", pointerEvents: "none" });
      $(".loader").show();
      console.log("start saving");
      send("write_all");
      const poll = setInterval(() => {
        if (rec.file !== null) {
          console.log("start saving");
          $(".loader").hide();
          finishBtn.css({ filter: "contrast(100%)", pointerEvents: "all" });
          clearInterval(poll);
        }
      }, 300);
    }
  });

  const checkRec = () => {
    if (gameState.stage !== "save") return;
    const { tmp_sound } = rec;
    sendOnce("write_play_3");
    if (!ltml) {
      ltml = new TimelineMax({ paused: true });
      ltml.to(path, tmp_sound.duration(), {
        strokeDashoffset: 0,
        ease: Power0.easeNone,
        onComplete: () => {
          ind.src = STATE_ICONS.PLAY;
          ltml.pause(0);
        }
      });
    }

    if (tmp_sound.playing()) {
      ind.src = STATE_ICONS.PLAY;
      tmp_sound.pause();
      ltml.pause();
    } else {
      ind.src = STATE_ICONS.PAUSE;
      tmp_sound.play();
      ltml.play();
    }
  };

  const toggleRec = () => {
    rec.toggleMicrophone();
    if (ptml.paused()) {
      $("#stopttp").hide();
      ptml.play();
      activeSound.play();
      send("write_play_2");
      ind.src = STATE_ICONS.PAUSE;
    } else {
      $("#stopttp").show();
      activeSound.pause();
      ptml.pause();
      send("write_pause");
      ind.src = STATE_ICONS.REC;
    }
  };

  const startRec = () => {
    record.find("h1").hide();
    if (gameState.story !== "none") {
      gameState.fairyTale = text2pages(ft[gameState.story]);
      readerText.get(0).innerText = gameState.fairyTale[0];
      reader.show();
    } else {
      $("#recOwl").show();
    }

    ptml.play(0);
    rec.toggleMicrophone();
    ind.src = STATE_ICONS.PAUSE;
    gameState.stage = "recording";
    if (gameState.sound === 0) {
      sounds.calm.play();
      activeSound = sounds.calm;
    } else {
      sounds.game.play();
      activeSound = sounds.game;
    }
  };

  next.on("click", () => {
    gameState.page += 1;
    readerText.get(0).innerText = gotoPage(gameState.page);
    prev.show();
    send("write_next_step");
    const nextPageAvailable = gotoPage(gameState.page + 1);
    if (!nextPageAvailable.length) {
      next.hide();
      ptml.pause(0);
      finishBtn.show();
      playBtn.hide();
      rec.saveFile();
      activeSound.pause();
      gameState.stage = "save";
      finishBtn.css({ filter: "contrast(50%)", pointerEvents: "none" });
      const poll = setInterval(() => {
        if (rec.file !== null) {
          finishBtn.css({ filter: "contrast(100%)", pointerEvents: "all" });
          clearInterval(poll);
        }
      }, 300);
    }
  });

  prev.on("click", () => {
    gameState.page -= 1;
    readerText.get(0).innerText = gotoPage(gameState.page);
    next.show();

    const prevPageAvailable = gotoPage(gameState.page - 1);
    if (!prevPageAvailable.length) {
      prev.hide();
    }
  });
}
