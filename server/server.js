let express = require("express");
let app = express();
let path = require("path");
let favicon = require('serve-favicon');
let bodyParser = require("body-parser");
let mailer = require('nodemailer');

module.authData = {
    host: "smtp.mailgun.org",
    login: 'postmaster@sandbox0c84c5a2c7a748289f2a99835568aa6e.mailgun.org',
    pass: '56d520e7a1e48adfd7fa9526d410c255-52b1d812-2316bc1a',
    apiKey: "key-4ec5aecc1bb369c658e51b2b72364267",
    receivers: ["vd@brainrus.ru", "welcome@brainrus.ru"]
};

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// create application/json parser
let jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
let urlencodedParser = bodyParser.urlencoded({extended: true});

app.use(favicon(path.join(__dirname, 'favicon', 'favicon.ico')));
app.use(express.static(__dirname + '/'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
    //__dirname : It will resolve to your project folder.
});


app.get('/ladavesta/*', function (req, res) {
    // static content

    let url = req.url;
    console.log(url);
    res.sendFile(path.join(__dirname + '/extras' + url));
});


app.get('/heinz/*', function (req, res) {
    // static content

    let url = req.url;
    console.log(url);
    res.sendFile(path.join(__dirname + '/extras' + url));
});


app.get('/olivehouse/*', function (req, res) {
    // static content

    let url = req.url;
    console.log(url);
    res.sendFile(path.join(__dirname + '/extras' + url));
});


app.get('/paintings/prostokvashino', function (req, res) {
    // static content

    let url = req.url;
    console.log(url);
    res.sendFile(__dirname + '/extras/paintings/1/index.html');
});

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

// POST '/' gets urlencoded bodies
app.post('/', urlencodedParser, function (req, res) {
    if (!req.body) return res.sendStatus(400);
    console.log('Client data transferred:', req.body);

    if (!validEmail(req.body.email)) return res.status(403).send("Email введен неверно");
    if (!req.body.message.length) return res.status(403).send("Сообщение введено неверно");

    let transporter = mailer.createTransport({
        service: "Mailgun",
        auth: {
            user: module.authData.login,
            pass: module.authData.pass
        }
    });

    let mailOptions = {
        from: req.body.email,
        to: module.authData.receivers.join(", "),
        subject: req.body.subject,
        text: req.body.message
        // attachments: [{filename: "data.xlsx", path: "./database/excel/data.xlsx"}]
    };


    transporter.sendMail(mailOptions, (err, response) => {
        if (err) {
            console.log(err);
            res.status(500).send('Возникла ошибка при отправке сообщения, попробуйте еще раз.');
        } else {
            console.log(response);
            res.status(200).send('Ваше сообщение доставлено');
        }
    });

});

app.get('/about', function (req, res) {
    res.sendFile(path.join(__dirname + '/about.html'));
});

app.get('/sitemap', function (req, res) {
    res.sendFile(path.join(__dirname + '/sitemap.html'));
});

app.listen(8124);
console.log("Running at Port 8124");
console.log(__dirname, ": PATH");


function validEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}