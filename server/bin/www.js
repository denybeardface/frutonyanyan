const http = require("https");
const fs = require("fs");
const app = require("../app");

var privateKey = fs.readFileSync(__dirname + "/key.pem").toString();
var certificate = fs.readFileSync(__dirname + "/cert.pem").toString();

const port = parseInt(process.env.PORT, 10) || 9999;
app.set("port", port);

const server = http.createServer(
  {
    key: privateKey,
    cert: certificate
  },
  app
);
server.listen(port);
