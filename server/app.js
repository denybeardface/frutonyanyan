const express = require("express");
const path = require("path");
const cors = require("cors");
const logger = require("morgan");
const bodyParser = require("body-parser");
const fs = require("fs");
const sox = require("sox.js");
const app = express();
const tasks = new Map();

const decodeChunk = b64string => {
  try {
    const [type, data] = b64string.split(",");
    if (data) return Buffer.from(data, "base64");
  } catch (err) {
    console.log("Not a b64 string!", err);
  }
};
app.set("view engine", "ejs");
app.use(logger("dev"));
app.use(cors());
app.use(bodyParser.urlencoded({ limit: "50mb", extended: false }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use("/audio", express.static(path.join(__dirname, "uploads")));

app.get("/", (req, res) => {
  console.log("working");
  res.send(200);
});

app.post("/uploadAudio", (req, res) => {
  const task_id = req.body.token;
  let entry = tasks.get(task_id);
  if (!entry) {
    tasks.set(task_id, {
      uploading: true
    });
  }

  const payload = decodeChunk(req.body.file_data);
  const filePath =
    //"/var/www/brainrus_nodejs/extras/sharedaudio/" +
    "uploads/" + task_id + "_" + req.body.file;
  fs.appendFileSync(filePath, payload);
  tasks.get(task_id).set({
    uploading: false
  });

  let timer_id = setTimeout(() => {
    let entry = tasks.get(task_id);
    if (!entry.uploading) {
      sox(
        {
          inputFile: filePath,
          outputFile: filePath.replace("wav", "mp3")
        },
        (err, outPath) => {
          console.log(err, outPath);
        }
      );
    }
  }, 3000);
  res.send(JSON.stringify({ url: `sharedaudio/${task_id}_${req.body.file}` }));
});

app.get("/uploadAudio/:file", (req, res) => {
  const file = req.params.file;
  res.render("audio", { file });
});

module.exports = app;
